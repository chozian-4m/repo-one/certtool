ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

# Add non root user
USER root 
RUN dnf install -y gnutls-utils && \
    dnf update -y && \
    dnf clean all && \
    rm -rf /var/cache/dnf && \
    groupadd -r certtool && \
    useradd -m -r certtool -g certtool
USER certtool

HEALTHCHECK CMD certtool || exit 0
ENTRYPOINT [ "/bin/sh" ]
