# Certtool

This project provides a Ubi8 container with certtool installed.

Certtool can parse and generate X.509 certificates, requests and private keys. It can be used interactively or non interactively by specifying the template command line option.

For more information visit their [man page](https://man7.org/linux/man-pages/man1/certtool.1.html) or run `$ certtool --help`
